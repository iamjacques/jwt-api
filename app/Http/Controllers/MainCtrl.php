<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Appointment as Appointment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MainCtrl extends Controller
{
    public function __construct()
   {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth', ['except' => ['importData']]);
   }

   /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        try{
        	
        	$users =  \App\User::all();
        	 return response()->json(['users' => $users], 200);

        } catch (ModelNotFoundException  $e) {
		 			
 			 return response()->json(['error' => 'could_not_find_users'], 500 ); 
 		}
    }



    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        try{
        	
        	$user = \App\User::create($request->all());
        	return response()->json(['status' => 'success'], 200 ); 

        } catch (\Exception  $e) {
		 			
 			return response()->json(['error' => 'could_not_create_user'], 500 ); 
 		}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request,$id)
    {
        //
        try{
        	
        	$user =  \App\User::findOrFail($id);
        	 return response()->json($user, 200);

        } catch (ModelNotFoundException  $e) {
		 			
 			 return response()->json(['error' => 'could_not_find_user'], 500 ); 
 		}
       
			
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
        	$user =  \App\User::findOrFail($id);

        	$street = $request->street;
	        $city = $request->city;
	        $zipcode = $request->zipcode;

	        // if the street, city or zipcode has been updated, get new coordinaes from google api 
	        if($street || $city || $zipcode){
	        	$params = $street .' '. $city .' ' . $zipcode;
	        	$client = new  \GuzzleHttp\Client();
				$res = $client->get('https://maps.googleapis.com/maps/api/geocode/json?&address='. $params);
				$res = json_decode($res->getBody(), true);
				$lat =  $res['results'][0]['geometry']['location']['lat'];
				$lng =  $res['results'][0]['geometry']['location']['lng'];
	        }

	        $user->name = $request->name;
			$user->username = $request->username;
			$user->email = $request->email;
			$user->password = Hash::make($request->email);
			$user->street = $street;
			$user->suite = $request->suite;
			$user->city = $city;
			$user->zipcode = $zipcode;
			$user->lat = $lat;
			$user->lng = $lng;
			$user->phone = $request->phone;
			$user->website = $request->website;
			$user->company_name = $request->name;
			$user->catchPhrase = $request->catchPhrase;
			$user->bs = $request->bs;
	 		$user->save();

        	return response()->json(['status' => 'success'], 200);

        } catch (\Exception $e) {

        	return response()->json(['error' => 'update_failed'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        try{
        	
        	$user =  \App\User::findOrFail($id);
        	$user->delete();
        	return response()->json(['status' => 'success'], 200);

        } catch (\Illuminate\Database\QueryException  $e) {
		 			
 			 return response()->json(['error' => 'could_not_delete_user'], 500 ); 
 		}
    }






  public function importData(){

  	$client = new  \GuzzleHttp\Client();
 	try {

		$res = $client->get('http://jsonplaceholder.typicode.com/users');

		if($res->getStatusCode() == 200){

		 	$users = json_decode($res->getBody(), true);

		 	for($i=0; $i < count($users); $i++){

		 		try {
			 		$user = new \App\User;
			 		$user->name = $users[$i]['name'];
					$user->username = $users[$i]['username'];
					$user->email = $users[$i]['email'];
					$user->password = Hash::make($users[$i]['email']);
					$user->street = $users[$i]['address']['street'];
					$user->suite = $users[$i]['address']['suite'];
					$user->city = $users[$i]['address']['city'];
					$user->zipcode = $users[$i]['address']['zipcode'];
					$user->lat = $users[$i]['address']['geo']['lat'];
					$user->lng = $users[$i]['address']['geo']['lng'];
					$user->phone = $users[$i]['phone'];
					$user->website = $users[$i]['website'];
					$user->company_name = $users[$i]['company']['name'];
					$user->catchPhrase = $users[$i]['company']['catchPhrase'];
					$user->bs = $users[$i]['company']['bs'];
			 		$user->save();
		 			
		 		} catch (\Illuminate\Database\QueryException  $e) {
		 			
		 			 return response()->json(['error' => 'could_not_create_users'], 500 ); 
		 		}
		 	}

			return response()->json(['status' => 'success'], 200);
			
		}
		

	} catch(\Exception $e){
         
		return response()->json(['error' => 'import_failed'], 500);
    }

  }



}
